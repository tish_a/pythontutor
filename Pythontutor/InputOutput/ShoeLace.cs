﻿using System;

namespace Pythontutor.InputOutput
{
    public class ShoeLace
    {
        public static void GetShoeLaceLength(int a, int b, int c, int d)
        {
           int shoeLaceLength = ((a + b) * 2)*(d - 1) + c * 2 + a;
            Console.WriteLine(shoeLaceLength);
        }
    }
}
