﻿using System;

namespace Pythontutor.InputOutput
{
    //Дано число n.С начала суток прошло n минут.Определите, сколько часов и минут будут показывать электронные часы
    //в этот момент.Программа должна вывести два числа: количество часов (от 0 до 23)
    //и количество минут(от 0 до 59). Учтите, что число n может быть больше, чем количество минут в сутках.
    static class DigitalWatch
    {
        public static void GetTime(int minutesPassed)
        {
            int hours = minutesPassed / 60 % 24;
            int minutes = minutesPassed % 60;
            Console.WriteLine($"{hours}:{ minutes}");
        }
    }
}
