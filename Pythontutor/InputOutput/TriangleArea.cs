﻿using System;

namespace Pythontutor.InputOutput
{
    static class TriangleArea 
    {
        public static void GetTriangleArea(double a, double b)
        {
            Console.WriteLine(a * b / 2);
        }
    }
}
