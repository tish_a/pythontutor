﻿using System;

namespace Pythontutor.InputOutput
{
    //В школе решили набрать три новых математических класса.Так как занятия по математике у них проходят в
    //одно и то же время,было решено выделить кабинет для каждого класса
    //и купить в них новые парты.За каждой партой может сидеть не больше двух учеников. Известно количество
    //учащихся в каждом из трёх классов. Сколько всего нужно закупить парт чтобы их хватило на всех учеников?
    //Программа получает на вход три натуральных числа: количество учащихся в каждом из трех классов.
    public static class Desks
    {
        public static void GetDesksCount(int studensInFirstClass, int studentsInSecondClass, int studentsInThirdClass)
        {
            int class1Desks = studensInFirstClass % 2 + studensInFirstClass/2;
            int class2Desks = studentsInSecondClass % 2 + studentsInSecondClass / 2;
            int class3Desks = studentsInThirdClass % 2 + studentsInThirdClass / 2;
            int totalDesks = class1Desks + class2Desks + class3Desks;
            Console.WriteLine(totalDesks);
        }
    }
}
