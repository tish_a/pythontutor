﻿using System;

namespace Pythontutor.InputOutput
{
    //n школьников делят k яблок поровну, неделящийся остаток остается в корзинке.
    //Сколько яблок достанется каждому школьнику? Сколько яблок останется в корзинке?
    //Программа получает на вход числа n и k и должна вывести искомое количество яблок (два числа).
    static class ApplesSharing
    {
        public static void GetApplesSharing(int students, int apples)
        {
            int apllesForStudents = apples / students;
            int remainApples = apples % students;
            Console.WriteLine($"{apllesForStudents}, {remainApples}");
        }

    }
}
