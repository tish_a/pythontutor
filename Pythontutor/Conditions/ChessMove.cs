﻿using System;

namespace Pythontutor.Conditions

{
    public class ChessMoves
    {
        public static bool CanRookMove(int curentPositionX, int curentPositionY, int afterMovePositionX, int afterMovePositionY)
        {
            var wasFigureMoved = WasChessMoved(curentPositionX, curentPositionY, afterMovePositionX, afterMovePositionY);
            if (wasFigureMoved && (afterMovePositionX == curentPositionX || curentPositionY == afterMovePositionY))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool CanKingMove(int curentPositionX, int curentPositionY, int afterMovePositionX, int afterMovePositionY)
        {
            int x = Math.Abs(curentPositionX - afterMovePositionX);
            int y = Math.Abs(curentPositionY - afterMovePositionY);
            var wasFigureMoved = WasChessMoved(curentPositionX, curentPositionY, afterMovePositionX, afterMovePositionY);
            if (wasFigureMoved 
                && ((x == 1 && y == 0)
                    || (x == 0 && y == 1)
                    || (x == 1 && y == 1)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool CanBishopMove(int curentPositionX, int curentPositionY, int afterMovePositionX, int afterMovePositionY)
        {
            int x = Math.Abs(curentPositionX - afterMovePositionX);
            int y = Math.Abs(curentPositionY - afterMovePositionY);
            var wasFigureMoved = WasChessMoved(curentPositionX, curentPositionY, afterMovePositionX, afterMovePositionY);
            if (wasFigureMoved == true && x == y)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool CanQueenMove(int curentPositionX, int curentPositionY, int afterMovePositionX, int afterMovePositionY)
        {
            return CanBishopMove(curentPositionX, curentPositionY, afterMovePositionX, afterMovePositionY) ||
            CanRookMove(curentPositionX, curentPositionY, afterMovePositionX, afterMovePositionY);
        }

        public static bool CanKnightMove(int curentPositionX, int curentPositionY, int afterMovePositionX, int afterMovePositionY)
        {
            int x = Math.Abs(curentPositionX - afterMovePositionX);
            int y = Math.Abs(curentPositionY - afterMovePositionY);
            var wasFigureMoved = WasChessMoved(curentPositionX, curentPositionY, afterMovePositionX, afterMovePositionY);
            if (wasFigureMoved == true && x == 1 && y == 2 || x == 2 && y == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool WasChessMoved(int curentPositionX, int curentPositionY, int afterMovePositionX, int afterMovePositionY)
        {
            if (curentPositionX == afterMovePositionX && curentPositionY == afterMovePositionY)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
