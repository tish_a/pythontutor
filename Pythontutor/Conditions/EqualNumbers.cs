﻿namespace Pythontutor.Conditions
{
    //Даны три целых числа.Определите, сколько среди них совпадающих. Программа должна вывести одно из чисел:
    //3 (если все совпадают), 2 (если два совпадает) или 0 (если все числа различны). 10 5 10
    public class EqualNumbers
    {
        public static int CheckNumbersEqual(int a, int b, int c)
        {
            if (a == b && a == c)
            {
                return 3;
            }
            else if (a == b || a == c || b == c)
            {
                return 2;
            }
            else return 0;
        }
    }
}
