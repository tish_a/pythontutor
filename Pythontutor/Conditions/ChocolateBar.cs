﻿namespace Pythontutor.Conditions
{
    public static class ChocolateBar
    {
        public static bool  CanChocolateBeSplitted(int sideN, int sideM, int squares)
        {
            if (squares <= sideN * sideM 
                && (squares % sideN == 0 || squares % sideM == 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}