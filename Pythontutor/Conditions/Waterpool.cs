﻿namespace Pythontutor.Conditions
{
    public static class Waterpool
    {
        static public int FindShortestDistance(int sizeX, int sizeY, int pointX, int pointY)
        {
            if (sizeX < sizeY)
            {
                int x1 = sizeX - pointX;
                int y1 = sizeY - pointY;
                if (x1 <= pointX && x1 <= pointY && x1 <= y1)
                    return x1;
                else if (pointX < x1 && pointX <= pointY && pointX <= y1)
                    return pointX;
                else if (pointY < pointX && pointY < x1 && pointY <= y1)
                    return pointY;
                else return y1;
            }
            else
            {
                int x1 = sizeY - pointX;
                int y1 = sizeX - pointY;
                if (x1 <= pointX && x1 <= pointY && x1 <= y1)
                    return x1;
                else if (pointX < x1 && pointX <= pointY && pointX <= y1)
                    return pointX;
                else if (pointY < pointX && pointY < x1 && pointY <= y1)
                    return pointY;
                else return y1;
            }
        }
    }
}
