﻿namespace Pythontutor.Conditions
{
    public static class MinimumOfThreeNumbers
    {
        public static int GetMinimumOfThreeNumbers(int a, int b, int c)
        {
            if (a <= b && a <= c)
            {
                return a;
            }
            else if (b <= a && b <= c)
            {
                return b;
            }
            else return c;

        }
    }
}
