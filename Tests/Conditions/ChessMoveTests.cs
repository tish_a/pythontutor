﻿using NUnit.Framework;
using Pythontutor.Conditions;

namespace Tests.Conditions
{
    [TestFixture]
    class ChessMoveTests
    {
        [Test]
        [TestCase(4, 4, 5, 5, false)]
        [TestCase(4, 4, 5, 4, true)]
        [TestCase(4, 4, 5, 3, false)]
        [TestCase(4, 4, 4, 5, true)]
        [TestCase(4, 4, 3, 5, false)]
        [TestCase(4, 4, 4, 3, true)]
        [TestCase(4, 4, 3, 4, true)]
        [TestCase(4, 4, 3, 3, false)]
        [TestCase(1, 1, 1, 8, true)]
        [TestCase(1, 1, 8, 8, false)]
        [TestCase(1, 1, 8, 1, true)]
        [TestCase(1, 8, 8, 8, true)]
        [TestCase(1, 8, 8, 1, false)]
        [TestCase(1, 8, 1, 1, true)]
        [TestCase(8, 8, 8, 1, true)]
        [TestCase(8, 8, 1, 1, false)]
        [TestCase(8, 8, 1, 8, true)]
        [TestCase(8, 1, 1, 1, true)]
        [TestCase(8, 1, 1, 8, false)]
        [TestCase(8, 1, 8, 8, true)]
        [TestCase(1, 1, 1, 2, true)]
        [TestCase(1, 1, 2, 2, false)]
        [TestCase(1, 1, 2, 1, true)]
        [TestCase(4, 4, 6, 6, false)]
        [TestCase(4, 4, 2, 2, false)]
        [TestCase(4, 4, 6, 2, false)]
        [TestCase(4, 4, 2, 6, false)]
        [TestCase(4, 4, 2, 7, false)]
        [TestCase(4, 4, 4, 6, true)]
        [TestCase(4, 4, 2, 4, true)]
        [TestCase(1, 2, 1, 3, true)]
        [TestCase(1, 2, 3, 1, false)]
        [TestCase(2, 1, 1, 3, false)]
        [TestCase(1, 1, 1, 1, false)]
        public void CanRookMove_WhenPassValues_ItShouldReturnCorrectResult(int curentPositionX, int curentPositionY, int afterMovePosotionX, int afterMovePosotionY,             bool expected)
        {
            // Arrange

            // Act
            var actual = ChessMoves.CanRookMove(curentPositionX, curentPositionY, afterMovePosotionX, afterMovePosotionY);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(4, 4, 5, 5, true)]
        [TestCase(4, 4, 5, 4, true)]
        [TestCase(4, 4, 4, 5, true)]
        [TestCase(4, 4, 3, 5, true)]
        [TestCase(4, 4, 4, 3, true)]
        [TestCase(4, 4, 3, 4, true)]
        [TestCase(4, 4, 3, 3, true)]
        [TestCase(4, 4, 3, 3, true)]
        [TestCase(1, 1, 1, 8, false)]
        [TestCase(1, 1, 8, 8, false)]
        [TestCase(1, 1, 8, 1, false)]
        [TestCase(1, 8, 8, 8, false)]
        [TestCase(1, 8, 8, 1, false)]
        [TestCase(1, 8, 1, 1, false)]
        [TestCase(8, 8, 8, 1, false)]
        [TestCase(8, 8, 1, 1, false)]
        [TestCase(8, 8, 1, 8, false)]
        [TestCase(8, 1, 1, 1, false)]
        [TestCase(8, 1, 1, 8, false)]
        [TestCase(8, 1, 8, 8, false)]
        [TestCase(1, 1, 1, 2, true)]
        [TestCase(1, 1, 2, 2, true)]
        [TestCase(1, 1, 2, 1, true)]
        [TestCase(4, 4, 6, 6, false)]
        [TestCase(4, 4, 2, 2, false)]
        [TestCase(4, 4, 6, 2, false)]
        [TestCase(4, 4, 2, 6, false)]
        [TestCase(4, 4, 2, 7, false)]
        [TestCase(4, 4, 4, 6, false)]
        [TestCase(4, 4, 2, 4, false)]
        [TestCase(4, 4, 5, 6, false)]
        [TestCase(1, 7, 1, 8, true)]
        [TestCase(4, 3, 2, 2, false)]
        [TestCase(1, 1, 1, 1, false)]
        public void CanKingMove_WhenPassValues_ItShouldReturnCorrectResult(int curentPositionX, int curentPositionY, int afterMovePosotionX, int afterMovePosotionY, bool expected)
        {
            // Arrange

            // Act
            var actual = ChessMoves.CanKingMove(curentPositionX, curentPositionY, afterMovePosotionX, afterMovePosotionY);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(4, 4, 5, 5, true)]
        [TestCase(4, 4, 5, 4, false)]
        [TestCase(4, 4, 5, 3, true)]
        [TestCase(4, 4, 4, 5, false)]
        [TestCase(4, 4, 3, 5, true)]
        [TestCase(4, 4, 4, 3, false)]
        [TestCase(4, 4, 3, 4, false)]
        [TestCase(4, 4, 3, 3, true)]
        [TestCase(1, 1, 1, 8, false)]
        [TestCase(1, 1, 8, 8, true)]
        [TestCase(1, 1, 8, 1, false)]
        [TestCase(1, 8, 8, 8, false)]
        [TestCase(1, 8, 8, 1, true)]
        [TestCase(1, 8, 1, 1, false)]
        [TestCase(8, 8, 8, 1, false)]
        [TestCase(8, 8, 1, 1, true)]
        [TestCase(8, 8, 1, 8, false)]
        [TestCase(8, 1, 1, 1, false)]
        [TestCase(8, 1, 1, 8, true)]
        [TestCase(8, 1, 8, 8, false)]
        [TestCase(1, 1, 1, 2, false)]
        [TestCase(1, 1, 2, 2, true)]
        [TestCase(1, 1, 2, 1, false)]
        [TestCase(4, 4, 6, 6, true)]
        [TestCase(4, 4, 2, 2, true)]
        [TestCase(4, 4, 6, 2, true)]
        [TestCase(4, 4, 2, 6, true)]
        [TestCase(4, 4, 2, 7, false)]
        [TestCase(4, 4, 4, 6, false)]
        [TestCase(4, 4, 2, 4, false)]
        [TestCase(7, 4, 2, 5, false)]
        [TestCase(7, 5, 1, 1, false)]
        [TestCase(1, 1, 1, 1, false)]
        public void CanBishopMove_WhenPassValues_ItShouldReturnCorrectResult(int curentPositionX, int curentPositionY, int afterMovePosotionX, int afterMovePosotionY, bool expected)
        {
            //Arange

            //Act 
            var actual = ChessMoves.CanBishopMove(curentPositionX, curentPositionY, afterMovePosotionX, afterMovePosotionY);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(1, 1, 2, 2, true)]
        [TestCase(1, 1, 2, 3, false)]
        [TestCase(5, 6, 3, 3, false)]
        [TestCase(3, 3, 1, 1, true)]
        [TestCase(6, 5, 2, 5, true)]
        [TestCase(7, 6, 5, 2, false)]
        [TestCase(2, 7, 6, 7, true)]
        [TestCase(2, 7, 4, 6, false)]
        [TestCase(7, 4, 2, 5, false)]
        [TestCase(7, 5, 1, 1, false)]
        [TestCase(2, 4, 5, 7, true)]
        [TestCase(3, 5, 7, 1, true)]
        [TestCase(5, 2, 5, 8, true)]
        [TestCase(1, 2, 3, 1, false)]
        [TestCase(2, 1, 1, 3, false)]
        public void CanQueenMove_WhenPassValues_ItShouldReturnCorrectResult(int curentPositionX, int curentPositionY, int afterMovePosotionX, int afterMovePosotionY, bool expected)
        {
            //Arange

            //Act 
            var actual = ChessMoves.CanQueenMove(curentPositionX, curentPositionY, afterMovePosotionX, afterMovePosotionY);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(1, 1, 1, 4, false)]
        [TestCase(1, 1, 8, 8, false)]
        [TestCase(2, 4, 3, 2, true)]
        [TestCase(5, 2, 4, 4, true)]
        [TestCase(2, 8, 3, 7, false)]
        [TestCase(2, 8, 3, 5, false)]
        [TestCase(5, 5, 3, 7, false)]
        [TestCase(2, 4, 2, 5, false)]
        [TestCase(4, 7, 6, 6, true)]
        [TestCase(4, 5, 2, 4, true)]
        [TestCase(2, 3, 3, 2, false)]
        [TestCase(5, 1, 4, 3, true)]
        [TestCase(6, 2, 8, 3, true)]
        [TestCase(1, 1, 1, 1, false)]
        public void CanKnightMove_WhenPassValues_ItShouldReturnCorrectResult(int curentPositionX, int curentPositionY, int afterMovePosotionX, int afterMovePosotionY, bool expected)
        {
            //Arange

            //Act 
            var actual = ChessMoves.CanKnightMove(curentPositionX, curentPositionY, afterMovePosotionX, afterMovePosotionY);

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
