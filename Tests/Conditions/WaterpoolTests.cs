﻿using NUnit.Framework;
using Pythontutor.Conditions;

namespace Tests.Conditions
{
    [TestFixture]
    public class WaterpoolTests
    {
        [Test]
        public void FindShortestDistance_WhenPassValues_ItShouldReturnCorrectResult_1()
        {
            // Arrange
            var n = 50;
            var m = 42;
            var x = 17;
            var y = 29;
            var expected = 17;

            // Act
            var actual = Waterpool.FindShortestDistance(n, m, x, y);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(23,52,8,43,8)]
        [TestCase(18,90,3,63,3)]
        [TestCase(96,1,0,83,0)]
        [TestCase(78,29,1,10,1)]
        [TestCase(49,31,14,32,14)]
        [TestCase(53,3,2,0,0)]
        [TestCase(73,63,51,8,8)]
        [TestCase(57,7,3,0,0)]
        [TestCase(54,22,15,6,6)]
        [TestCase(50,42,17,29,17)]
        [TestCase(43,33,6,30,6)]
        [TestCase(65,30,13,12,12)]
        [TestCase(11,47,1,20,1)]
        [TestCase(5,98,2,81,2)]
        [TestCase(34,39,3,7,3)]
        [TestCase(45,48,17,11,11)]
        [TestCase(78,48,0,4,0)]
        [TestCase(90,72,9,35,9)]
        [TestCase(16,100,7,11,7)]
        [TestCase(38,31,2,4,2)]
        [TestCase(96,45,41,22,4)]
        [TestCase(3,56,0,37,0)]
        [TestCase(28,25,17,28,0)]
        [TestCase(6,97,6,95,0)]
        [TestCase(88,18,7,70,7)]
        [TestCase(25,57,0,20,0)]
        [TestCase(97,38,6,38,6)]
        [TestCase(98,77,31,80,18)]
        [TestCase(41,84,4,73,4)]
        [TestCase(46,90,28,77,13)]
        [TestCase(5,94,1,36,1)]
        [TestCase(66,45,35,47,10)]
        [TestCase(60,98,39,27,21)]
        [TestCase(67,22,7,54,7)]
        [TestCase(6,38,1,16,1)]
        [TestCase(72,19,9,42,9)]
        [TestCase(79,78,49,79,0)]
        [TestCase(71,26,21,42,5)]
        [TestCase(5,87,3,38,2)]
        [TestCase(31,79,0,74,0)]
        public void FindShortestDistance_WhenPassValues_ItShouldReturnCorrectResult_2(int n, int m, int x, int y, int expected)
        {
            // Arrange

            // Act
            var actual = Waterpool.FindShortestDistance(n, m, x, y);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}